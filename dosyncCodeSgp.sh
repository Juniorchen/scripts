#!/bin/bash

sync_dryrun()
{
	echo
	echo -e "\033[1m ...dryrun...\033[0m"
	rsync -vr --delete ~/scripts/server root@188.166.246.17:/var/www/html/ --dry-run
	echo -e "\033[1m ...dryrun...\033[0m"
	echo
}

# sync_dryrun 188.166.246.17

sync_server()
{
	echo
	echo -e "\033[1m ...syncing...\033[0m"
	rsync -arv --progress --delete ~/scripts/server root@188.166.246.17:/var/www/html/
	echo -e "\033[1m ...done..\033[0m"
}


	sync_server
