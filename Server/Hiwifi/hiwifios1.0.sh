
#!/bin/sh
cd /tmp
echo "*********************************************************"
echo "*                                                       *"
echo "*                                                       *"
echo "*                                                       *"
echo "*             SS插件安装器                              *"
echo "*            安装前请关闭极路由自带SSH/VPN功能          *"
echo "*         只测试过极路由9009-9018版本，其他自测         *"
echo "*             技术支持QQ：000000000000                  *"
echo "*             技术支持QQ群： 000000000                  *"
echo "*********************************************************"
echo "                                                         "
echo "请选择需要的操作（按下对应数字后回车确认）"
echo "1：安装SS插件"
echo "2：卸载SS插件"
echo "0:退出"
read num

if [ "${num}" == "1" ]
then
cd /tmp
rm -rf *.sh
curl -k -o install.sh https://sspanel.tabboa.com/new/install1.0.sh
chmod -R 777 install.sh
./install.sh
fi
if [ "${num}" == "2" ]
then
cd /tmp
rm -rf *.sh
curl -k -o uninstall.sh https://sspanel.tabboa.com/new/uninstall.sh
chmod -R 777 uninstall.sh
./uninstall.sh
fi

if [ "${num}" == "0" ]
then
cd /tmp
rm -rf *.sh
curl -k -o sstool.sh https://sspanel.tabboa.com/new/sstool.sh
chmod 777 sstool.sh
sh sstool.sh
fi

    



