#!/bin/bash
#格式化SD卡,烧录系统到SD卡
#启动系统，打开ssh
#homebridge安装及插件安装
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install git make
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get install libavahi-compat-libdnssd-dev
# Install Homebridge and dependencies
# (note that /usr/local/lib should be replaced with /usr/lib/ if you installed node using apt-get method above)

sudo npm install -g --unsafe-perm homebridge hap-nodejs node-gyp
cd /usr/lib/node_modules/homebridge/
sudo npm install --unsafe-perm bignum
cd /usr/lib/node_modules/hap-nodejs/node_modules/mdns
sudo node-gyp BUILDTYPE=Release rebuild

#Running Homebridge on Boot (/etc/rc.local) using Screen
# If you would like your Pi to start up HomeBridge automatically in a Screen session on reboot, you need to install Screen and edit the Pi's /etc/rc.local file.
# Install Screen

sudo apt-get install screen
# Edit /etc/rc.local

# sudo nano /etc/rc.local
# Add this line before the exit 0 line:

# su -c "screen -dmS homebridge homebridge" -s /bin/sh pi
# Hit Ctrl+X, Y to save and exit.
#shairPort

#安装homebridge GPIO
npm install -g homebridge-gpio
git clone git://github.com/jamesblanksby/quick2wire-gpio-admin.git
cd quick2wire-gpio-admin
make
sudo make install
sudo adduser $USER gpio

#安装homebridge people
npm install -g homebridge-people
#安装homebridge cmd
npm install -g homebridge-cmd
#安装homebridge wemo
npm install -g homebridge-platform-wemo
#安装homebridge ifttt
npm install -g homebridge-ifttt

sudo apt-get install samba samba-common-bin
#shairport
sudo apt-get install git libao-dev libssl-dev libcrypt-openssl-rsa-perl \
    libio-socket-inet6-perl libwww-perl avahi-utils libmodule-build-perl

git clone https://github.com/njh/perl-net-sdp.git perl-net-sdp
cd perl-net-sdp
perl Build.PL
sudo ./Build
sudo ./Build test
sudo ./Build install
cd ..

git clone https://github.com/hendrikw82/shairport.git
cd shairport
./configure --prefix=/usr
make
sudo make install

#自启动,添加到/etc/rc.local
# su -c "screen -dmS homebridge homebridge" -s /bin/sh pi
# su -c "screen -dmS shairport shairport --buffer=400 -a 'AirPi'" -s /bin/sh pi
#LAMP
# PHP, MySQL, and Apache2 uses only package installers, namely apt-get and Pip
#Step 1 — Installing Apache 2
# sudo apt-get update
# sudo apt-get install apache2

# #Step2 Install MySQL
# sudo apt-get install mysql-server php5-mysql
# sudo mysql_install_db
# sudo mysql_secure_installation

# #Step 3 Install PHP
# sudo apt-get install php5 libapache2-mod-php5 php5-mcrypt
# #Install PHP Modules
# sudo apt-get install php5-curl php5-cli