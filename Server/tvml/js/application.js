var resourceLoader;

App.onLaunch = function(options) {
	var javascriptFiles = [
	 `${options.BASEURL}js/ResourceLoader.js`,
	 `${options.BASEURL}js/Presenter.js`
	];

	evaluateScripts(javascriptFiles, function(success) {
		if(success) {
			resourceLoader = new ResourceLoader(options.BASEURL);
			resourceLoader.loadResource(`${options.BASEURL}templates/RWDevConTemplate.xml.js`, function(resource) {
				var doc = Presenter.makeDocument(resource);
				doc.addEventListener("select", Presenter.load.bind(Presenter));
				Presenter.pushDocument(doc);
			})
		} else {
			var errorDoc = createAlert("Evaluate Scripts Error", "Error attempting to evaluate external JavaScript files.");
			navigationDocument.presentModal(errorDoc);
		}
	});
}
App.onWillEnterForeground = function() {
	requestGetData();
}

function requestGetData{
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        var DONE = this.DONE || 4;
        if (this.readyState === DONE){
            alert(this.readyState);
        }
    };
    request.open('GET', 'http://app.pearvideo.com/clt/jsp/v4/myFollowContList.jsp', true);
    request.setRequestHeader('Cookie', 'JSESSIONID=E3EA2A5BD37BD2A5E59F461C8E74DAD3; __ads_session=7pHppW8gGAlLSPR5CwA=; PEAR_PLATFORM=1; PEAR_TOKEN=b32c8135-29df-4caa-aebb-5e7bbfb7e93c; PEAR_UID="P26czkD9ia13rlRsBQ5Ruw=="; PEAR_UUID=BE3306BE-9D64-4DA5-A85A-4FD3ABA0013A; PV_APP=srv-pv-prod-portal4; Hm_lvt_9707bc8d5f6bba210e7218b8496f076a=1506779069; PEAR_DEVICE_FLAG=true; PEAR_UUID=4557eb0e-89c3-478e-8ae6-b65cadc557fa'); 
    request.send(null);
}

var createAlert = function(title, description) {
	var alertString = `<?xml version="1.0" encoding="UTF-8" ?>
		<document>
			<alertTemplate>
				<title>${title}</title>
				<description>${description}</description>
				<button>
					<text>OK</text>
				</button>
			</alertTemplate>
		</document>`
	var parser = new DOMParser();
	var alertDoc = parser.parseFromString(alertString, "application/xml");
	return alertDoc
}