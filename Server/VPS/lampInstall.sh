#!/bin/bash

# PHP, MySQL, and Apache2 uses only package installers, namely apt-get and Pip


#Step 1 — Installing Apache 2
sudo apt-get update
sudo apt-get install apache2

#Step2 Install MySQL
sudo apt-get install mysql-server php5-mysql
sudo mysql_install_db
sudo mysql_secure_installation

#Step 3 Install PHP
sudo apt-get install php5 libapache2-mod-php5 php5-mcrypt
#Install PHP Modules
sudo apt-get install php5-curl php5-cli