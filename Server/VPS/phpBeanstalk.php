<?php

use Beanstalk\Client;

//
// A sample producer.
//
$beanstalk = new Client(); // For connection options see the
                           // class documentation.

$beanstalk->connect();
$beanstalk->useTube('flux'); // Begin to use tube `'flux'`.
$beanstalk->put(
    23, // Give the job a priority of 23.
    0,  // Do not wait to put job into the ready queue.
    60, // Give the job 1 minute to run.
    '/path/to/cat-image.png' // The job's body.
);
$beanstalk->disconnect();

//
// A sample consumer.
//
$beanstalk = new Client();

$beanstalk->connect();
$beanstalk->watch('flux');

while (true) {
    $job = $beanstalk->reserve(); // Block until job is available.
    // Now $job is an array which contains its ID and body:
    // ['id' => 123, 'body' => '/path/to/cat-image.png']

    // Processing of the job...
    $result = touch($job['body']);

    if ($result) {
        $beanstalk->delete($job['id']);
    } else {
        $beanstalk->bury($job['id']);
    }
}
// When exiting i.e. on critical error conditions
// you may also want to disconnect the consumer.
// $beanstalk->disconnect();

?>