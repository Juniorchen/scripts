var infoData;
var list;
var allVideos;
var followVideos = [];
var collectionList = [];
var hotVideos = [];
var userList;
var homeBannerList;
var releatVideos;
var followUsers=[];
var urlDic = {'1':'2'};
function requestGetUserInfo(userID){
   var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState === 4){
        	var json=JSON.parse(request.responseText);  
        	infoData = json
          getLatestVideo(userID);
        }
    };
    var url = 'https://baobab.kaiyanapp.com/api/v4/pgcs/detail/tab?id='+userID;
    request.open('GET', url, true);
    request.send(null);

}

function getLatestVideo (userID) {
      var request1 = new XMLHttpRequest();
    request1.onreadystatechange = function () {
        if (this.readyState === 4){
          var json=JSON.parse(request1.responseText);
          list = json['itemList'];
              requestGetAllVideos(userID);
        }
    };
    var url = 'https://baobab.kaiyanapp.com/api/v4/pgcs/detail/index?id='+userID;
    request1.open('GET', url, true);
    request1.send(null);
  // body...
}

function requestGetAllVideos(userID){
   var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState === 4){
        	var json=JSON.parse(request.responseText);  
        	allVideos = json['itemList'];
        	processTemplate(json);
        }
    };
    var url = 'https://baobab.kaiyanapp.com/api/v4/pgcs/videoList?start=0&num=1000&id='+userID;
    request.open('GET', url, true);
    request.send(null);
}

//获取首页视频
function requestGetHome(){
	   var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState === 4){
        	var json=JSON.parse(request.responseText);  
        	homeBannerList = json['itemList'];
          requestGetCollectionList();
        	        	
        }
    };
    var url = 'https://baobab.kaiyanapp.com/api/v4/tabs/selected';
    request.open('GET', url, true);
    request.send(null);
}

//获取热门视频
function requestReleate(player,videoID){
	   var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState === 4){
        	var json=JSON.parse(request.responseText);  
        	releatVideos = json['itemList'];
          player.interactiveOverlayDocument = processOverlayDoc();
        }
    };
    var url = 'https://baobab.kaiyanapp.com/api/v4/video/related?id='+videoID;
    request.open('GET', url, true);
    request.send(null);
}

//获取热门视频
function requestGetHot(){
     var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState === 4){
          var json=JSON.parse(request.responseText);  
          hotVideos = json['itemList'];
          hotVideos.splice(0,9);
          requestGetMyFollowUsers();
        }
    };
    var url = 'https://baobab.kaiyanapp.com/api/v4/discovery/hot?start=0&num=500';
    request.open('GET', url, true);
    request.send(null);
}

//获取分类视频
function requestGetCategoryList(){
	   var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState === 4){
        	var json=JSON.parse(request.responseText);  
        	allVideos = json['itemList'];
        	processTemplate(json);
        }
    };
    var url = 'https://baobab.kaiyanapp.com/api/v4/discovery/category?start=0&num=20';
    request.open('GET', url, true);
    request.send(null);
}

//获取作者
function requestGetAuthors(){
var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState === 4){
        	var json=JSON.parse(request.responseText);  
        	userList = json['itemList'];
        	requestGetHome();
        }
    };
    var url = 'https://baobab.kaiyanapp.com/api/v4/pgcs/all?start=0&num=1000';
    request.open('GET', url, true);
    request.send(null);
}

function requestGetMyFollowUsers(){
       var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState === 4 && request.responseText.length){
          var json=JSON.parse(request.responseText);  
          followUsers = json['itemList'];
              processHomeTemplate();
        }
    };
    var url = 'http://188.166.246.17:3000?type=followUserList';
    request.open('GET', url, true);
    request.send(null);
}
	//需要用cookie所以要自己写API了

function requestGetMyFollows(){
		   var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState === 4 && request.responseText.length){
        	var json=JSON.parse(request.responseText);  
        	followVideos = json['itemList'];
          requestGetAuthors();
        }
    };
    var url = 'http://188.166.246.17:3000?type=followList';
    request.open('GET', url, true);
    request.send(null);
}
	//需要用cookie所以要自己写API了

function requestFollowUser(userID){
	//需要用cookie所以要自己写API了
var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState === 4){
        	var json=JSON.parse(request.responseText);  
        	//replace 一下 document
        }
    };
    var url = 'https://baobab.kaiyanapp.com/api/v1/follow/add?itemType=author&itemId='+userID;
    request.open('GET', url, true);
    request.send(null);
}
	//需要用cookie所以要自己写API了

function requestGetCollectionList(userID){
var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState === 4){
        	var json=JSON.parse(request.responseText);  
    collectionList = json['itemList'];
    requestGetHot();
        }
    };
    var url = 'http://188.166.246.17:3000?type=collectionList';
    request.open('GET', url, true);
    request.send(null);
}


function processTemplate(json){
	var data = infoData['pgcInfo'];
	var docText = `<document>
  <head>
    <style>
    .showTextOnHighlight {
      tv-text-highlight-style: show-on-highlight;
    }
    .badge {
      tv-tint-color: rgb(0,0,0);
    }
    .9ColumnGrid {
      tv-interitem-spacing: 51;
    }
    </style>
  </head>
  <productTemplate theme="dark">

    <banner>
      <infoList>
        <info>
          <header>
            <title>genre</title>
          </header>
          <text>Genre Name</text>
        </info>
        <info>
          <header>
            <title>director</title>
          </header>
          <text>Director Name</text>
        </info>
      </infoList>
      <stack>
        <title>${data['name']}</title>
        <!-- In lieu of title an image can be used of size max dimensions of 846x130 -->
        <!-- <img src="" width="846.0" height="130.0" /> -->
        <row>
          <text><badge src="resource://tomato-fresh" style="margin:0 0 -3"/> 90%</text>
          <text>${data['brief']}</text>
          <badge src="resource://mpaa-pg" class="badge" />
          <badge src="resource://cc" class="badge" />
        </row>
        <description handlesOverflow="true">${data['description']}</description>
        <row>
          <buttonLockup>
            <badge src="resource://button-rate" />
            <title>收藏视频</title>
          </buttonLockup>
          <buttonLockup>
            <badge src="resource://button-checkmark" />
            <title>已关注</title>
          </buttonLockup>
          <buttonLockup>
            <badge src="resource://button-add" />
            <title>加关注</title>
          </buttonLockup>
        </row>
      </stack>
		<heroImg src="${data['icon']}" width="400" height="600" />
    </banner>`;

    for(var group of list){
    	if (group['type'] == 'videoCollectionOfHorizontalScrollCard') {
    		docText += `
    <shelf class="9ColumnGrid">
      <header>
        <title>${group['data']['header']['title']}</title>
      </header>
      <section>`;
      for(var item of group['data']['itemList']){
      	        
                            docText += processVideoTemplate(item,docText);

      }
      docText +=      ` </section>
    </shelf>`;
    	};
    }

    docText += `
    <shelf class="9ColumnGrid">
      <header>
        <title>全部视频</title>
      </header>
      <section>`;
      var i=0;
    for(var item of allVideos){
                                  docText += processVideoTemplate(item,docText,'allVideo',i);
                                  i++;
    }
    docText +=      ` </section>
    </shelf>`;
    
    docText +=`
    <shelf>

      <header>
        <title>Reviews &amp; Ratings</title>
      </header>
      <section>
        <ratingCard>
          <title>4.1 / 5</title>
          <ratingBadge value="0.7"></ratingBadge>
          <description>Average of 2,241 iTunes user ratings and reviews.</description>
        </ratingCard>
        <ratingCard>
          <title><badge src="resource://tomato-splat-m" /> 41%</title>
          <text>Tomatometer</text>
          <infoTable>
            <info>
              <header>
                <title>175</title>
              </header>
              <text>Reviews</text>
            </info>
            <info>
              <header>
                <title>88</title>
              </header>
              <text>Fresh</text>
            </info>
            <info>
              <header>
                <title>87</title>
              </header>
              <text>Rotten</text>
            </info>
            <info>
              <header>
                <title>6</title>
              </header>
              <text>Average</text>
            </info>
          </infoTable>
        </ratingCard>
        <reviewCard>
          <badge src="resource://tomato-fresh-m" />
          <title>Publisher</title>
          <description>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</description>
          <text>Stephanie Vidal Dec, 24 2015</text>
        </reviewCard>
        <reviewCard>
          <badge src="resource://tomato-splat-m" />
          <title>Publisher</title>
          <description>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</description>
          <text>Euna Kwon Dec, 5 2015</text>
        </reviewCard>
        <reviewCard>
          <badge src="resource://tomato-certified-m" />
          <title>Publisher</title>
          <description>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</description>
          <text>Paul Cashman <date>Dec, 10 2015</date></text>
        </reviewCard>
      </section>
    </shelf>
    <shelf>
      <header>
        <title>Shelf Header</title>
      </header>
      <section>
        <monogramLockup>
          <monogram src="/resources/images/lockups/square_1.jpg" />
          <title>Adam Gooseff</title>
          <subtitle>Actor</subtitle>
        </monogramLockup>
        <monogramLockup>
          <monogram src="/resources/images/lockups/square_2.jpg" />
          <title>Ailish Kimber</title>
          <subtitle>Actor</subtitle>
        </monogramLockup>
        <monogramLockup>
          <monogram firstName="Allen" lastName="Buchinski" />
          <title>Allen Buchinski</title>
          <subtitle>Actor</subtitle>
        </monogramLockup>
        <monogramLockup>
          <monogram firstName="Dave" lastName="Elfving" />
          <title>Dave Elfving</title>
          <subtitle>Actor</subtitle>
        </monogramLockup>
        <monogramLockup>
          <monogram firstName="Ethan" lastName="Izzarelli" />
          <title>Ethan Izzarelli</title>
          <subtitle>Actor</subtitle>
        </monogramLockup>
        <monogramLockup>
          <monogram firstName="Euna" lastName="Kwon" />
          <title>Euna Kwon</title>
          <subtitle>Director</subtitle>
        </monogramLockup>
        <monogramLockup>
          <monogram firstName="Fritz" lastName="Ogden" />
          <title>Fritz Ogden</title>
          <subtitle>Director</subtitle>
        </monogramLockup>
        <monogramLockup>
          <monogram firstName="Gilbert" lastName="Solano" />
          <title>Gilbert Solano</title>
          <subtitle>Director</subtitle>
        </monogramLockup>
      </section>
    </shelf>

    <separator />

    <productInfo>
      <infoTable>
        <header>
          <title>Header</title>
        </header>

        <info>
          <header>
            <title>Header</title>
          </header>
          <text>Text</text>
        </info>

        <info>
          <header>
            <title>Header</title>
          </header>
          <text>Text</text>
        </info>
        <info>
          <header>
            <title>Header</title>
          </header>
          <text>Text</text>
        </info>
        <info>
          <header>
            <title>Header</title>
          </header>
          <text>Text</text>
        </info>
        <info>
          <header>
            <title>Header</title>
          </header>
          <text>Text</text>
        </info>
        <info>
          <header>
            <title>Header</title>
          </header>
          <text>Text</text>
        </info>
        <footer>
          <text>Footer Text</text>
        </footer>
      </infoTable>
      <infoTable>
        <header>
          <title>Header</title>
        </header>
        <info>
          <header>
            <title>Primary&#xD;Additional</title>
          </header>
          <description handlesOverflow="true">English (Dolby 5.1), Subtitles, CC&#xD;Arabic (Subtitles)&#xD;Cantonese (Subtitles)&#xD;Croatian (Subtitles)&#xD;Czech (Subtitles)&#xD;Danish (Subtitles)&#xD;Dutch (Subtitles)&#xD;Finnish (Subtitles)&#xD;French (Subtitles)&#xD;Arabic (Subtitles)&#xD;Cantonese (Subtitles)&#xD;Croatian (Subtitles)&#xD;Czech (Subtitles)&#xD;Danish (Subtitles)&#xD;Dutch (Subtitles)&#xD;Finnish (Subtitles)&#xD;French (Subtitles)
          </description>
        </info>
      </infoTable>
      <infoTable style="tv-line-spacing:10;">
        <header>
          <title>Header</title>
        </header>
        <info>
          <header>
            <textBadge>SDH</textBadge>
          </header>
          <text>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</text>
        </info>
        <info>
          <header>
            <textBadge>AD</textBadge>
          </header>
          <text>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</text>
        </info>
      </infoTable>
    </productInfo>
  </productTemplate>
</document>`;
console.log(docText);
        var currentHomeDoc = (new DOMParser).parseFromString(docText, "application/xml");
        navigationDocument.pushDocument(currentHomeDoc);

        currentHomeDoc.addEventListener("play", function(event) {
        playKaiYanVideos(parseInt(event.target.getAttribute('index')));
    });
}

function processUrl(id,playInfo,item){
	var length = playInfo.length;
	if (length) {
			var lasturl = playInfo[length-1];
	// console.log(lasturl['url']);
	urlDic[id] = lasturl['url'];
	}else{
		urlDic[id] = item['playUrl'];
	}
  return urlDic[id];
}

function processOverlayDoc(){
	var docText = `<document>
  <head>
    <style>
      * {
        <!-- To cutomize the interval image is displayed on screen -->
        tv-transition-interval:8.0;
      }
    </style>
  </head>

  <stackTemplate>
      <collectionList>`;
    
docText+=`
      <shelf>
        <header>
          <title>相关视频</title>
        </header>

        <section>`;
              for(var item of releatVideos){
        if (item['type'] == 'videoSmallCard') {
        docText += processVideoTemplate(item,docText);
      };
}
console.log(docText);
        docText +=`</section>
      </shelf></collectionList>
  </stackTemplate>

</document>`;
	var currentHomeDoc = (new DOMParser).parseFromString(docText, "application/xml");

        return currentHomeDoc;
}

function playVideo(title,id){

	var video = new MediaItem('video', urlDic[id]);
video.title = title;
//解决经常出现恢复观看时间的弹窗
// video.resumeTime = 10.0; // seconds
var playlist = new Playlist();
playlist.push(video);
 

var  player = new Player();
player.playlist = playlist;
// player.interactiveOverlayDocument = processOverlayDoc();
// player.overlayDocument = processOverlayDoc();
player.play(); // Present the player
player.addEventListener("stateDidChange", function(event) {
        console.log("Event: " + event.type + "\ntarget: " + event.target + "\ntimestamp: " + event.timeStamp + "\noldState: " + event.oldState + "\nnew state: " + event.state);
        if (event.oldState == 'end' && event.state == 'loading') {
            requestReleate(player,id);
        };
    });

player.addEventListener("timeDidChange", function(event) {
        console.log("Event: " + event.type + "\ntarget: " + event.target + "\ntimestamp: " + event.timeStamp + "\ntime: " +  event.time + "\ninterval: " + event.interval);
    }, { interval: 10 });
}

function processHomeTemplate(){
	var docText = `<document> <head>
		<style>
			.carouselOverlay {
				padding:180 90 0 1100;
			}
			.carouselOverlayImg {
				tv-position: top-left; 
				margin: 0 0 0 0;
			}
			.carouselOverlayTitle {
				tv-position: top;
				tv-align: left; 
				margin: 36 0 0;
			}
			.carouselOverlaySubtitle {
				tv-position: top; 
				tv-align: left;
			}
			.carouselOverlayText {
				tv-position: footer; 
				tv-align: left; 
				tv-text-max-lines: 3; 
				margin: 0 0 38;
			}

			.carouselOverlayImg2 {
				tv-position: bottom-left; 
				margin: 0 20 0 0;
			}

			.progressOverlay {
				padding: 0;
			}
			.watchedOverlayImg {
				tv-position: bottom-right;
				tv-align: right;
			}

			.imgGradient {
				tv-tint-color: linear-gradient(top, 0.37, transparent, 0.5, rgba(61,101,128,0.5), 0.96, rgba(61,101,128,1.0), rgba(61,101,128,1.0));
			}
			.gradientOverlayTitle {
				tv-text-max-lines: 1;
				tv-text-style: body;
				padding: 30;
				text-align: center;
				color: rgba(255, 255, 255, 1.0);
			}
			.gradientOverlaySubtitle {
				color: rgba(255, 255, 255, 0.6);
				tv-text-style: caption2;
				tv-text-max-lines: 1;
				margin: 6 0 -6 0;
				text-align: center;
			}

			.simpleCard {
				width: 548;
				height: 308;
				background-color: rgba(255, 255, 255, 0.5);
                tv-highlight-color: rgba(255, 255, 255, 0.9);

			}
			.simpleCardTitle {
				tv-position: center;
				color: rgba(0, 0, 0, 0.6);
				tv-text-style: headline;
				tv-text-max-lines: 1;
				text-align: center;
			}
			.simpleCardSubtitle {
				tv-position: center;
				color: rgba(0, 0, 0, 0.4);
				tv-text-style: subtitle3;
				tv-text-max-lines: 1;
				text-align: center;
			}

			<!-- Music mini Card -->
            .musicMiniCard {
                width:666;
                height:308;
                border-radius: 12;
                background-color: rgba(255, 255, 255, 0.7);
            }
            .musicMiniImage {
                width:308;
                height:308;
                tv-position:left;
                border-radius: 12 0 0 12;
            }
            .musicMiniTitle {
                text-align: left;
                tv-text-max-lines: 2;
                tv-position:top;
                tv-align: left;
                margin: 32 30 0;
                tv-text-style:callout;
                color: rgba(0, 0, 0, 0.6);
            }
            .musicMiniSubtitle {
                text-align: left;
                tv-text-max-lines: 1;
                tv-position:top;
                tv-align: left;
                margin: 32 30 0;
                tv-text-style:subhead;
                color: rgba(0, 0, 0, 0.4);
            }
            .musicMiniDescription {
                text-align: left;
                tv-text-max-lines: 3;
                tv-position:top;
                tv-align: left;
                margin: 9 30 0;
                tv-text-style:caption1;
                color: rgba(0, 0, 0, 0.4);
            }
		</style>
	</head>
	<stackTemplate>
		<collectionList>
			<carousel>
				<section>`;
				for(var banner of homeBannerList){
					if (banner['type'] == 'video') {
						var bannerData = banner['data'];
            var icon = '';
            if (bannerData['author']) {
              icon = bannerData['author']['icon'];
            };
                  processUrl(banner['data']['id'],banner['data']['playInfo'],banner['data']);
            var title = processStr(banner['data']['title']);
            var slogan = processStr(bannerData['slogan']);
            var desc = processStr(bannerData['description']);
						docText += `<lockup onselect="playVideo('${title}','${banner['data']['id']}')">
						<img src="${bannerData['cover']['feed']}" width="1740" height="900" />
						<overlay class="carouselOverlay">
							<img class="carouselOverlayImg" src="${icon}" width="160" height="160"/>
							<title class="carouselOverlayTitle">${title}</title>
							<subtitle class="carouselOverlaySubtitle">${slogan}</subtitle>
							<text class="carouselOverlayText">${desc}</text>
						</overlay>
					</lockup>`;
					};
				}
					
	docText+=`
				</section>
			</carousel>`;
    docText+=`
      <shelf>
        <header>
          <title>最新发布</title>
        </header>

        <section>`;
              for(var item of hotVideos){
        if (item['type'] == 'video') {

        docText += processVideoTemplate(item,docText);
      }
}
      docText+=`
        </section>
        </shelf>`;

  docText+=`
			<shelf>
				<header>
					<title>我的关注</title>
				</header>

				<section>`;
					    for(var item of followVideos){
        if (item['type'] == 'followCard') {

				docText += processVideoTemplate(item['data']['content'],docText);
      }
}
console.log(docText);

      docText+=`
        </section>
        </shelf>`;


      docText +=`
      <shelf>
        <header>
          <title>我的收藏</title>
        </header>
        <section>`;
              for(var item of collectionList){
        if (item['type'] == 'video') {

        docText += processVideoTemplate(item,docText);
      }
}
console.log(docText);
        docText +=`</section>
      </shelf>`;
      docText += `<shelf>
      <header>
        <title>关注的作者</title>
      </header>
      <section>`;

      for(var user of followUsers){
        if (user['type'] == 'briefCard') {
          userInfo = user['data'];
          var title = processStr(userInfo['title']);
        docText +=  `<monogramLockup onselect="requestGetUserInfo('${userInfo['id']}')"><monogram  src="${userInfo['icon']}"/>
          <title>${title}</title>
          <subtitle>Actor</subtitle>
        </monogramLockup>`;
        };
      }
      docText +=`</section>
      </shelf>`;
			docText += `<shelf>
      <header>
        <title>全部作者</title>
      </header>
      <section>`;

      for(var user of userList){
      	if (user['type'] == 'briefCard') {
      		userInfo = user['data'];
          var title = processStr(userInfo['title']);
      	docText +=  `<monogramLockup onselect="requestGetUserInfo('${userInfo['id']}')"><monogram  src="${userInfo['icon']}"/>
          <title>${title}</title>
          <subtitle>Actor</subtitle>
        </monogramLockup>`;
        };
      }
      
 
		docText+=`     </section>
    </shelf></collectionList>
	</stackTemplate>
	</document>`;
	console.log(docText);
       var currentHomeDoc = (new DOMParser).parseFromString(docText, "application/xml");
        navigationDocument.pushDocument(currentHomeDoc);
        currentHomeDoc.addEventListener("play", function(event) {
        console.log(event);
    });
        currentHomeDoc.addEventListener("holdselect", function(event) {
        console.log(event);
        requestGetUserInfo(event.target.getAttribute('value'));
    });
}

// 注意有标题特殊字符引起的问题
function processStr(str){
  return str=str.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\:|\"|\'|\,|\<|\.|\>|\/|\?]/g,"");

}

function playKaiYanVideos(index){
    var playlist = new Playlist();

  for (var i = index; (i < allVideos.length)&&(i<index+20); i++) {
      var item = allVideos[i];
      var title = processStr(item['data']['title']);
      var url =   processUrl(item['data']['id'],item['data']['playInfo'],item['data']);
;
      var video = new MediaItem('video', url);
      video.title = title;
      video.artworkImageURL = item['data']['cover']['feed'];
    //解决经常出现恢复观看时间的弹窗
    // video.resumeTime = 10.0; // seconds
      playlist.push(video);
  };

 
  var player = new Player();
  player.playlist = playlist;
// player.modalOverlayDocument = processOverlayDoc();
  // player.interactiveOverlayDocument = processOverlayDoc();
  // player.overlayDocument = processOverlayDoc();
  player.play(); // Present the player
}

function processVideoTemplate(item,docText,videoType,index){
	if (item) {
		var video = {'title':item['data']['title']};
    var icon = '';
    var userid = '';
    if (item['data']['author']) {
      icon = item['data']['author']['icon'];
      userid = item['data']['author']['id'];
    };
    var title = processStr(item['data']['title']);
    	processUrl(item['data']['id'],item['data']['playInfo'],item['data']);
      	var tmpText = `<lockup onselect="playVideo('${title}','${item['data']['id']}')" value="${userid}" videoType='${videoType}' index='${index}'>
          <img src="${item['data']['cover']['feed']}" width="384" height="216" />
          						<overlay>
							      <img class="carouselOverlayImg" src="${icon}" width="80" height="80"/>
						</overlay>
                    <title class="showTextOnHighlight">${title}</title>

        </lockup>`;
        return tmpText;
	};
	return '';
    	
}