/*
Copyright (C) 2016 Apple Inc. All Rights Reserved.
See LICENSE.txt for this sample’s licensing information

Abstract:
This class handles presenting the Search template example.
*/

class SearchController extends DocumentController {

    setupDocument(document) {
        super.setupDocument(document);

        // Obtain references to some useful elements in the searchTemplate
        const searchTemplateElem = document.getElementsByTagName('searchTemplate').item(0);
        const searchFieldElem = document.getElementsByTagName('searchField').item(0);
        const separatorElem = document.getElementsByTagName('separator').item(0);
        const messageElem = document.getElementById("message");
        const suggestionsElem = document.getElementById("suggestions");
        const defaultResultsElem = document.getElementById("defaultResults");
        const resultsModeElem = document.getElementById("resultsMode");
        const resultsListElem = document.getElementById("resultsList");
        const resultsShelfContainerElem = document.getElementById("resultsShelfContainer");
        const resultsGridContainerElem = document.getElementById("resultsGridContainer");
        const resultsSectionElem = document.getElementById("resultsSection");

        // Clean up the document for initial presentation
        let resultsContainerElem = resultsShelfContainerElem;
        resultsListElem.removeChild(resultsGridContainerElem);
        toggleDefaultResults(true);

        // Define some search-related constants
        const searchBaseURL = "https://itunes.apple.com/search?";
        const searchResultsLimit = 25;
        const searchMediaType = "movie";
        const searchMediaArtworkWidth = 250;
        const searchMediaArtworkAspectRatio = 1.5;
        const searchMediaTitleFunc = function(item) {
            return item.trackCensoredName || item.trackName ||
                   item.collectionCensoredName || item.collectionName;
        };
        const searchMediaItemIDFunc = function(item) {
            return item.trackId || item.collectionId;
        };
        const searchMediaArtworkURLFunc = function(item) {
            let result = item.artworkUrl60;
            if (typeof result === "string") {
                // WORKAROUND: Obtain high-resolution artwork
                result = result.replace("/60x60", "/600x600");
            }
            return result;
        };
        loginPearVideo();
        requestGetData();

        // Retain the XHR between searches so it can be cancelled mid-flight
        let searchRequest;
        // Retain the query between searches to determine changes
        let searchTextCache;
        // Retain the result items to handle selection events
        const searchResultsCache = [];

        const searchKeyboard = searchFieldElem.getFeature("Keyboard");
        // Register an event handler for search input changes
        searchKeyboard.onTextChange = performSearchRequest;

        // Register an event handler for search result mode selection
        resultsModeElem.addEventListener("highlight", function(event) {
            const selectedElement = event.target;
            const selectedMode = selectedElement.getAttribute("value");
            setResultsMode(selectedMode);
        });

        // Register an event handler for search suggestions selection
        suggestionsElem.addEventListener("select", function(event) {
            const selectedElement = event.target;
            const searchValue = selectedElement.getAttribute("value");
            searchKeyboard.text = searchValue;
            performSearchRequest();
        });

        // Register an event handler for search result selection
        resultsSectionElem.addEventListener("select", function(event) {
            const selectedElement = event.target;
            const resultIndex = selectedElement.getAttribute("resultIndex");
            const resultItem = searchResultsCache[resultIndex];
            handleSelectionForItem(resultItem);
        });
function requestGetData(){
    var request = new XMLHttpRequest();

    request.open('GET', 'http://baobab.wandoujia.com/api/v3/videos?_s=78f8613f969646454b522b58f53e4fe3&categoryId=6&f=iphone&net=wifi&num=100&start=0&strategy=date&u=131a21991850a2161db717faa79ea11af0c07cc3&v=2.1.0&vc=611', true);
    request.setRequestHeader("aa",'asdafsf');
    request.onreadystatechange = function () {
        var DONE = this.DONE || 4;
        if (this.readyState === DONE){
            // alert(this.readyState);
        }
        if (this.readyState === 4){
            var json=JSON.parse(request.responseText);  
            var list = json['itemList'];
            // alert(this.readyState);
            var docText = `
            <document>
              <head>
                <style>
                .overlay_title {
                    background-color: rgba(0,0,0,0.6);
                    color: #FFFFFF;
                    text-align: center;
                }
                .overlay {
                    padding: 0;
                    margin: 0;
                }
                .5ColumnGrid {
      tv-interitem-spacing: 50;
    }
                .title{
                    tv-text-highlight-style:marquee-on-highlight;
                    show-on-highlight:marquee-and-show-on-highlight;
                }
                </style>
              </head>
  <stackTemplate theme="dark">
                  <banner>
                     <title>TV源</title>
                  </banner>
                  <collectionList>
      <grid class="5ColumnGrid">
        <section>`;
        for(var data of list) {
            var value = data['data'];
                // docText += `<lockup><img src="${value['cover']['feed']}" width="300" height="200" />
                // <overlay class="overlay"><title class="overlay_title">${value['title']}</title>
                //                     </overlay>
                //                     </lockup>`;

                docText += `<lockup>
                <title class='title'>${value['title']}</title>
                <mediaContent playbackMode="onFocus">
      <img src="${value['cover']['feed']}" width="548" height="308"/>
   </mediaContent>
                                    </lockup>`;


        }
        docText += `
                </section>
                     </grid>
                  </collectionList>
               </stackTemplate>
            </document>`;
        console.log("doc: "+docText);
        var currentHomeDoc = (new DOMParser).parseFromString(docText, "application/xml");
        var i = 0;
        for(var data of list) {
                        var value = data['data'];
        var mediaContentElement = currentHomeDoc.getElementsByTagName('mediaContent').item(i);
var player = mediaContentElement.getFeature('Player');
var playlist = new Playlist();
var video = new MediaItem('video', value['playUrl']);
video.artworkImageURL = value['cover']['feed'];
video.title = value['title'];
video.description = value['description'];
video.resumeTime = 10.0; // seconds
playlist.push(video);
player.playlist = playlist;
i++;
}
        

        currentHomeDoc.addEventListener('select', function(event) {
            var overlayDocText = '<document><title>测试overlay</title></document>';
                    var overlayDoc = (new DOMParser).parseFromString(overlayDocText, "application/xml");

   var mediaContentElement = event.target.getElementsByTagName('mediaContent').item(0);
   var player = mediaContentElement.getFeature('Player');
   player.overlayDocument = overlayDoc;
   player.present();
});
        navigationDocument.pushDocument(currentHomeDoc);

        }
    };
    // request.setRequestHeader("Cookie", 'JSESSIONID=E3EA2A5BD37BD2A5E59F461C8E74DAD3; __ads_session=7pHppW8gGAlLSPR5CwA=; PEAR_PLATFORM=1; PEAR_TOKEN=b32c8135-29df-4caa-aebb-5e7bbfb7e93c; PEAR_UID="P26czkD9ia13rlRsBQ5Ruw=="; PEAR_UUID=BE3306BE-9D64-4DA5-A85A-4FD3ABA0013A; PV_APP=srv-pv-prod-portal4; Hm_lvt_9707bc8d5f6bba210e7218b8496f076a=1506779069; PEAR_DEVICE_FLAG=true; PEAR_UUID=4557eb0e-89c3-478e-8ae6-b65cadc557fa'); 
    request.send(null);
}
function loginPearVideo(){
    var request = new XMLHttpRequest();

    request.open('POST', 'http://app.pearvideo.com/clt/v4/societyFromSDK.msp', true);
    request.setRequestHeader("aa",'asdafsf');
    request.onreadystatechange = function () {
        var DONE = this.DONE || 4;
        if (this.readyState === DONE){
            // alert(this.readyState);
        }
        if (this.readyState === 4){
            // alert(this.readyState);
            console.log(document.cookie);
        }
    };
    // request.setRequestHeader("Cookie", 'JSESSIONID=E3EA2A5BD37BD2A5E59F461C8E74DAD3; __ads_session=7pHppW8gGAlLSPR5CwA=; PEAR_PLATFORM=1; PEAR_TOKEN=b32c8135-29df-4caa-aebb-5e7bbfb7e93c; PEAR_UID="P26czkD9ia13rlRsBQ5Ruw=="; PEAR_UUID=BE3306BE-9D64-4DA5-A85A-4FD3ABA0013A; PV_APP=srv-pv-prod-portal4; Hm_lvt_9707bc8d5f6bba210e7218b8496f076a=1506779069; PEAR_DEVICE_FLAG=true; PEAR_UUID=4557eb0e-89c3-478e-8ae6-b65cadc557fa'); 
    request.send("name=Juniorchen2012&otype=1&pic=https%3A//tvax1.sinaimg.cn/crop.0.18.925.925.180/66d53610ly8fmu0vcb54jj20pp0qpwk0.jpg&sex=2&societyType=SINA&token=2.007FyksB09EBwZ186238a2e32eEuJC&tokenSecret=&uid=1725249040");
}
        /*
         * Show or hide the message in the search body.
         * Sets the content of the message if it is to be shown.
         */
        function toggleSearchMessage(bool, message) {
            if (bool) {
                // Set the message text
                if (message) {
                    messageElem.textContent = message;
                }
                // Show the message if it's hidden
                if (!messageElem.parentNode) {
                    searchTemplateElem.appendChild(messageElem);
                }
                toggleModeButtons(false);
            } else {
                // Hide the message if it's visible
                if (messageElem.parentNode) {
                    searchTemplateElem.removeChild(messageElem);
                }
                toggleModeButtons(true);
            }
        }

        function toggleSearchSuggestions(bool) {
            if (bool) {
                // Show the suggestions if they're hidden
                if (!suggestionsElem.parentNode) {
                    searchTemplateElem.appendChild(suggestionsElem);
                }
                toggleSearchMessage(false);
                toggleModeButtons(false);
            } else {
                // Hide the suggestions if they're visible
                if (suggestionsElem.parentNode) {
                    searchTemplateElem.removeChild(suggestionsElem);
                }
                toggleModeButtons(true);
            }
        }

        function toggleDefaultResults(bool) {
            if (bool) {
                // Swap the default results in for the container
                if (resultsContainerElem.parentNode) {
                    resultsListElem.removeChild(resultsContainerElem);
                    resultsListElem.appendChild(defaultResultsElem);
                }
                toggleSearchMessage(false);
                toggleSearchSuggestions(false);
                toggleModeButtons(false);
            } else {
                // Swap the default results out and the container in
                if (!resultsContainerElem.parentNode) {
                    resultsListElem.removeChild(defaultResultsElem);
                    resultsListElem.appendChild(resultsContainerElem);
                }
                toggleModeButtons(true);
            }
        }

        function toggleModeButtons(bool) {
            if (bool) {
                if (!separatorElem.parentNode) {
                    searchTemplateElem.appendChild(separatorElem);
                }
            } else {
                if (separatorElem.parentNode) {
                    searchTemplateElem.removeChild(separatorElem);
                }
            }
        }

        function setResultsMode(mode) {
            // Remove existing results container
            while (resultsListElem.firstChild) {
                resultsListElem.removeChild(resultsListElem.firstChild);
            }
            // Determine the new results container element
            if (mode === "shelf") {
                resultsContainerElem = resultsShelfContainerElem;
            }
            if (mode === "grid") {
                resultsContainerElem = resultsGridContainerElem;
            }
            // Show the new results container in the collectionList
            resultsListElem.appendChild(resultsContainerElem);
            resultsContainerElem.appendChild(resultsSectionElem);
        }

        function performSearchRequest() {
            // Strip leading, trailing, and multiple whitespaces from the query
            const searchText = searchKeyboard.text.trim().replace(/\s+/g, " ");

            // Do nothing if the query hasn't meaningfully changed
            if (searchTextCache && searchText === searchTextCache) {
                return;
            }
            // Retain this query for the next time the input changes
            searchTextCache = searchText;

            // If there's already a search in-progress, cancel it.
            if (searchRequest && searchRequest.readyState !== XMLHttpRequest.DONE) {
                searchRequest.abort();
            }

            // Show the initial message and stop if there's no search query
            if (searchText.length === 0) {
                toggleDefaultResults(true);
                return;
            }

            // Build the URL for the search query
            const searchParams = [
                `media=${searchMediaType}`,
                `limit=${searchResultsLimit}`,
                `term=${encodeURIComponent(searchText)}`
            ].join("&");
            const searchURL = searchBaseURL + searchParams;

            // Perform the search request
            searchRequest = new XMLHttpRequest();
            searchRequest.open("GET", searchURL);
            searchRequest.responseType = "json";
            searchRequest.onload = showSearchResponse;
            searchRequest.onerror = showSearchError;
            searchRequest.send();

            searchFieldElem.setAttribute("showSpinner", true);
        }

        /*
         * Show a generic error message in the search body
         */
        function showSearchError() {
            toggleSearchMessage(true, "An error occurred during your search.");
            searchFieldElem.setAttribute("showSpinner", false);
        }

        /*
         * Parse the XHR response and show the results or a message
         */
        function showSearchResponse() {
            // Prepare the document for new search results
            toggleDefaultResults(false);
            toggleSearchSuggestions(false);
            clearSearchResults();
            searchFieldElem.setAttribute("showSpinner", false);

            // Show the results (or lack thereof)
            const searchResponse = searchRequest.response;
            const searchResults = searchResponse.results;
            if (searchResults.length > 0) {
                appendSearchResults(searchResults);
                toggleSearchMessage(false);
            } else {
                if (searchTextCache.length > 3) {
                    toggleSearchMessage(true, `No results for ${searchTextCache}.`);
                } else {
                    toggleSearchSuggestions(true);
                }
            }
        }

        /*
         * Empty the results cache and remove all results lockup elements.
         */
        function clearSearchResults() {
            searchResultsCache.length = 0;
            // Remove all existing search results
            while (resultsSectionElem.firstChild) {
                resultsSectionElem.removeChild(resultsSectionElem.firstChild);
            }
        }

        /*
         * Create lockup elements for the search results and cache
         * the data to be referenced by the selection handler.
         */
        function appendSearchResults(results) {
            const startIndex = searchResultsCache.length;
            // Create new lockups for the results
            results.forEach(function(item, index) {
                index += startIndex;
                // Create item lockup element
                const lockupElem = document.createElement("lockup");
                // Set the result array index on the lockup
                lockupElem.setAttribute("resultIndex", index);
                // Populate the lockup element details
                populateLockupWithItem(lockupElem, item);
                // Add the lockup to the results collection
                resultsSectionElem.appendChild(lockupElem);
                // Add the item to the search results cache
                searchResultsCache.push(item);
            });
        }

        /*
         * Inserts elements containing data from a search result into the
         * empty lockup element created to display the result.
         * Shows an image, title, and subtitle in the lockup.
         */
        function populateLockupWithItem(lockupElem, item) {
            // Determine the lockup image attributes
            const titleText = searchMediaTitleFunc(item);
            const itemID = searchMediaItemIDFunc(item);
            const imgURL = searchMediaArtworkURLFunc(item);
            const imgWidth = searchMediaArtworkWidth;
            const imgHeight = imgWidth * searchMediaArtworkAspectRatio;
            // Create the child nodes of the lockup element
            const imgElem = document.createElement("img");
            const titleElem = document.createElement("title");
            // Set the lockup image attributes
            imgElem.setAttribute("src", imgURL);
            imgElem.setAttribute("width", imgWidth);
            imgElem.setAttribute("height", imgHeight);
            // Set the lockup element text from the item
            titleElem.setAttribute("class", "showTextOnHighlight");
            titleElem.textContent = titleText;
            // Put the child nodes into the lockup
            lockupElem.setAttribute("itemID", itemID);
            lockupElem.appendChild(imgElem);
            lockupElem.appendChild(titleElem);
        }

        /*
         * Called when a search result is selected, passing in the
         * JSON Object that was returned by the API for the result.
         */
        function handleSelectionForItem(item) {
            console.log("handleSelectionForItem: " + JSON.stringify(item));
            // Do something more interesting than logging
        }
    }

}

registerAttributeName('searchDocumentURL', SearchController);
