#!/bin/bash

sync_dryrun()
{
	echo
	echo -e "\033[1m ...dryrun...\033[0m"
	rsync -vr --delete ~/scripts/server root@chaohappy.com:/var/www/html/ --dry-run
	echo -e "\033[1m ...dryrun...\033[0m"
	echo
}

# sync_dryrun

sync_server()
{
	echo
	echo -e "\033[1m ...syncing...\033[0m"
	rsync -arv --progress --delete ~/scripts/server root@chaohappy.com:/var/www/html/
	echo -e "\033[1m ...done..\033[0m"
}


	sync_server
